package be.multimedi.restSamples.webshop.controllers;

import be.multimedi.restSamples.webshop.entities.ProductCategory;
import be.multimedi.restSamples.webshop.models.CategoryList;
import be.multimedi.restSamples.webshop.repositories.ProductCategoryDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/categories")// localhost:8080/categories/1
public class CategoryController {

   @Autowired
   ProductCategoryDAO dao;

   @GetMapping(produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
   public ResponseEntity<CategoryList> getAllHandler(){
      return ResponseEntity.ok(new CategoryList(dao.findAll()));
   }

   @GetMapping(path = "/{id:^\\d+$}", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
   public ResponseEntity<ProductCategory> getByIdHandler(@PathVariable("id") int id){
      ProductCategory category = dao.getProductCategoryById(id);
      return category != null ? ResponseEntity.ok(category) : ResponseEntity.badRequest().build(); //ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
   }

   @DeleteMapping(path = "/{id:^\\d+$}")
   public ResponseEntity<?> deleteHandler(@PathVariable("id") int id ){
      dao.deleteById(id);
      return ResponseEntity.ok().build();
   }

   @PostMapping(consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE },
                  produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
   public ResponseEntity<ProductCategory> postHandler( @RequestBody ProductCategory category ){
      return category != null && category.getId() <= 0 ? ResponseEntity.ok(dao.save(category)) : ResponseEntity.badRequest().build();
   }
}
