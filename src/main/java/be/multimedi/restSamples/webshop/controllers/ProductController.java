package be.multimedi.restSamples.webshop.controllers;

import be.multimedi.restSamples.webshop.entities.Product;
import be.multimedi.restSamples.webshop.models.ProductList;
import be.multimedi.restSamples.webshop.repositories.BrandDAO;
import be.multimedi.restSamples.webshop.repositories.ProductCategoryDAO;
import be.multimedi.restSamples.webshop.repositories.ProductDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/products")
public class ProductController {

   @Autowired
   ProductCategoryDAO categoryDAO;

   @Autowired
   BrandDAO brandDAO;

   @Autowired
   ProductDAO dao;

   @GetMapping(produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
   public ResponseEntity<ProductList> getAllHandler() {
      return ResponseEntity.ok(new ProductList(dao.findAll()));
   }

   @GetMapping(path = "/{id:^\\d+$}", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
   public ResponseEntity<Product> getByIdHandler(@PathVariable("id") int id) {
      Product p = dao.getProductById(id);
      return p != null ? ResponseEntity.ok(p) : ResponseEntity.notFound().build();
   }

   @DeleteMapping(path = "/{id:^\\d+$}")
   public ResponseEntity<?> deleteHandler(@PathVariable("id") int id) {
      dao.deleteById(id);
      return ResponseEntity.ok().build();
   }

   @PostMapping(produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
           consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
   public ResponseEntity<Product> postHandler(@RequestBody Product p){
      if(p!=null && p.getId()<=0){
         dao.save(p);
         p.setBrand(brandDAO.getBrandById(p.getBrand().getId()));
         p.setCategory(categoryDAO.getProductCategoryById(p.getCategory().getId()));
         return ResponseEntity.ok(p);
      }
      return ResponseEntity.badRequest().build();
   }
}
