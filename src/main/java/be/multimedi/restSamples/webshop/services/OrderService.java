package be.multimedi.restSamples.webshop.services;

import be.multimedi.restSamples.webshop.entities.Order;

public interface OrderService {
   int orderProduct(String email, int productId, int amount);
   int orderProducts(String email, int[][] products);
   Order placeOrder(Order order);
}
