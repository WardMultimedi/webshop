package be.multimedi.restSamples.webshop.services;

import be.multimedi.restSamples.webshop.entities.Order;
import be.multimedi.restSamples.webshop.entities.OrderItem;
import be.multimedi.restSamples.webshop.entities.Product;
import be.multimedi.restSamples.webshop.entities.User;
import be.multimedi.restSamples.webshop.repositories.OrderDAO;
import be.multimedi.restSamples.webshop.repositories.OrderItemDAO;
import be.multimedi.restSamples.webshop.repositories.ProductDAO;
import be.multimedi.restSamples.webshop.repositories.UserDAO;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class OrderServiceImpl implements OrderService{
   @Autowired
   ProductDAO productDAO;

   @Autowired
   OrderItemDAO orderItemDAO;

   @Autowired
   OrderDAO orderDAO;

   @Autowired
   UserDAO userDAO;

   @Override
   public int orderProduct(String email, int productId, int amount) {
      User user = userDAO.getUserByEmail(email);
      if(user==null){
         return 0;
      }
      System.out.println("email OK");

      Product p = productDAO.getProductById(productId);
      if(p==null){
         return 0;
      }
      System.out.println("product OK");
      if(amount>0 && p.getStock() >= amount){
         p.setStock(p.getStock() - amount);

         Order order = new Order();
         order.setDate(LocalDateTime.now());
         order.setUser(user);
         order = orderDAO.save(order);

         List<OrderItem> items = new ArrayList<OrderItem>();
         OrderItem item = new OrderItem(p, order, amount);
         orderItemDAO.save(item);
         items.add(item);
         return order.getId();
      }

      return 0;
   }

   @Override
   public int orderProducts(String email, int[][] products) {
      User user = userDAO.getUserByEmail(email);
      if(user==null){
         return 0;
      }

      List<OrderItem> items = new ArrayList<OrderItem>();
      Order order = new Order();
      for(int i = 0; i<products.length; i++){
         if(products[i].length != 2)
            return 0;
         int productId = products[i][0];
         Product p = productDAO.getProductById(productId);

         if(p == null){
            return 0;
         }

      }
      order.setDate(LocalDateTime.now());
      order.setUser(user);
      orderDAO.save(order);
      for(int i = 0; i<products.length; i++){
         int productId = products[i][0];
         Product p = productDAO.getProductById(productId);
         int amount = products[i][1];

         if(amount>0 && amount<= p.getStock()){
            p.setStock(p.getStock() - amount);
            OrderItem item = new OrderItem(p, order, amount);
            orderItemDAO.save(item);
            items.add(item);
         }
      }
      return 0;
   }

   @Override
   public Order placeOrder(Order order) {
      if(order == null) return null;
      if(order.getId() <= 0){
         if(order.getUser() == null) return null;
         List<OrderItem> items = order.getItems();
         //# check if ordered products is valid
         for(OrderItem item : items){
            Product p = item.getProduct();
            if(p == null) return null;
            if(p.getStock() > item.getAmount()) return null;
         }
         //# order products
         for(OrderItem item : items){
            Product p = item.getProduct();
            p.setStock(p.getStock() - item.getAmount());
         }
         return orderDAO.save(order);
      }
      return null;
   }
}
