package be.multimedi.restSamples.webshop.models;

import be.multimedi.restSamples.webshop.entities.Order;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class OrderList {
   List<Order> orders;

   public OrderList(List<Order> orders) {
      this.orders = orders;
   }

   public OrderList() {
   }

   public List<Order> getOrders() {
      return orders;
   }

   public void setOrders(List<Order> orders) {
      this.orders = orders;
   }
}
