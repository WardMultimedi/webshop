package be.multimedi.restSamples.webshop.models;

import be.multimedi.restSamples.webshop.entities.OrderItem;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class OrderItemList {
   List<OrderItem> items;

   public OrderItemList(List<OrderItem> items) {
      this.items = items;
   }

   public OrderItemList() {
   }

   public List<OrderItem> getItems() {
      return items;
   }

   public void setItems(List<OrderItem> items) {
      this.items = items;
   }
}
