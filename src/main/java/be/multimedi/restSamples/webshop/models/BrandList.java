package be.multimedi.restSamples.webshop.models;

import be.multimedi.restSamples.webshop.entities.Brand;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class BrandList {
   //@JsonProperty("brandlist")
   List<Brand> brands;

   public BrandList() {
   }

   public BrandList(List<Brand> brands) {
      this.brands = brands;
   }

   public List<Brand> getBrands() {
      return brands;
   }

   public void setBrands(List<Brand> brands) {
      this.brands = brands;
   }
}
