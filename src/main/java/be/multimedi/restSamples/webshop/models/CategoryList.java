package be.multimedi.restSamples.webshop.models;

import be.multimedi.restSamples.webshop.entities.ProductCategory;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class CategoryList {
   List<ProductCategory> categories;

   public CategoryList(List<ProductCategory> categories) {
      this.categories = categories;
   }

   public CategoryList() {
   }

   public List<ProductCategory> getCategories() {
      return categories;
   }

   public void setCategories(List<ProductCategory> categories) {
      this.categories = categories;
   }
}
