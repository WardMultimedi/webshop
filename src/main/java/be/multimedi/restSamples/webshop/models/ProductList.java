package be.multimedi.restSamples.webshop.models;

import be.multimedi.restSamples.webshop.entities.Product;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class ProductList {
   List<Product> products;

   public ProductList(List<Product> products) {
      this.products = products;
   }

   public ProductList() {
   }

   public List<Product> getProducts() {
      return products;
   }

   public void setProducts(List<Product> products) {
      this.products = products;
   }
}
