package be.multimedi.restSamples.webshop.repositories;

import be.multimedi.restSamples.webshop.entities.Brand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BrandDAO extends JpaRepository<Brand, Integer> {
   default Brand getBrandById(Integer integer) {
      return findById(integer).orElse(null);
   }
}
