package be.multimedi.restSamples.webshop.repositories;

import be.multimedi.restSamples.webshop.entities.OrderItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderItemDAO extends JpaRepository<OrderItem, Integer> {
}
