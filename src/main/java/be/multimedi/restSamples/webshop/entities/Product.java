package be.multimedi.restSamples.webshop.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name= "Products")
@JsonIgnoreProperties(value="hibernateLazyInitializer")
public class Product {

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private int id;

   @NotBlank
   @Column(nullable = false)
   private String model;

   @Min(0)
   @Column(nullable = false)
   private float price;

   @Min(0)
   @Column(nullable = false)
   private int stock;

   @ManyToOne
//   @Column(nullable = false)
   @JoinColumn(name = "categoryId", nullable = false)
   private ProductCategory category;

   @ManyToOne
   @JoinColumn( name="brandId", nullable = false)
   private Brand brand;

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getModel() {
      return model;
   }

   public void setModel(String model) {
      this.model = model;
   }

   public float getPrice() {
      return price;
   }

   public void setPrice(float price) {
      this.price = price;
   }

   public int getStock() {
      return stock;
   }

   public void setStock(int stock) {
      this.stock = stock;
   }

   public ProductCategory getCategory() {
      return category;
   }

   public void setCategory(ProductCategory category) {
      this.category = category;
   }

   public Brand getBrand() {
      return brand;
   }

   public void setBrand(Brand brand) {
      this.brand = brand;
   }
}
