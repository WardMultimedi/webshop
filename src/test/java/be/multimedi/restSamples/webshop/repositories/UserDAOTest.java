package be.multimedi.restSamples.webshop.repositories;

import be.multimedi.restSamples.webshop.entities.Product;
import be.multimedi.restSamples.webshop.entities.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class UserDAOTest {
   @Autowired
   UserDAO dao;

   @Test
   public void testGetAll(){
      List<User> users = dao.findAll();

      assertNotNull(users);
      assertEquals(3, users.size());
   }

   @Test
   public void testGetById(){
      User user = dao.getUserByEmail("ward@multimedi.be");

      assertNotNull(user);
      assertEquals("Ward", user.getFirstname());
      assertEquals("Truyen", user.getLastname());
      assertEquals("lol", user.getPassword());
      assertEquals("javastraat", user.getStreet());
      assertEquals("11A", user.getBusNr());
      assertEquals("3000", user.getZipcode());
      assertEquals("Leuven", user.getCity());
      assertEquals("Belgium", user.getCountry());
   }

   //@DirtiesContext
   @Test
   @Rollback
   public void testSave(){
      User user = new User();
      user.setEmail("john@doe.com");
      user.setPassword("lol");
      user.setFirstname("foo");
      user.setLastname("bar");

      dao.save(user);
      List<User> users = dao.findAll();

      assertNotNull(users);
      assertEquals(4, users.size());
   }

   //@DirtiesContext
//   @Test
//   @Rollback
//   public void testDeleteById(){
//      dao.deleteById("ward@multimedi.be");
//      List<User> users = dao.findAll();
//
//      assertNotNull(users);
//      assertEquals(2, users.size());
//   }
}
