-- HyperSQL
CREATE TABLE IF NOT EXISTS Brands  (
    id          INTEGER IDENTITY NOT NULL,
    name        VARCHAR(32) NOT NULL,
    website     VARCHAR(64)
);

CREATE TABLE IF NOT EXISTS ProductCategories  (
    id          INTEGER IDENTITY NOT NULL,
    name        VARCHAR(32) NOT NULL,
    description VARCHAR(128)
);

CREATE TABLE IF NOT EXISTS WebshopUsers  (
    email       VARCHAR(64) PRIMARY KEY NOT NULL,
    password    VARCHAR(32) NOT NULL,
    firstname   VARCHAR(32) NOT NULL,
    lastname    VARCHAR(32) NOT NULL,
    street      VARCHAR(32),
    busNr       VARCHAR(10),
    zipcode     VARCHAR(10),
    city        VARCHAR(32),
    country     VARCHAR(24)
);

CREATE TABLE IF NOT EXISTS Products  (
    id          INTEGER IDENTITY NOT NULL,
    model       VARCHAR(32),
    price       DECIMAL(6,2),
    stock       INTEGER NOT NULL,
    brandId     INTEGER NOT NULL,
    categoryId  INTEGER NOT NULL,
    CONSTRAINT FK_Products_Brands FOREIGN KEY(brandId) REFERENCES Brands(id),
    CONSTRAINT FK_Products_Categories FOREIGN KEY(categoryId) REFERENCES ProductCategories(id)
);

CREATE TABLE IF NOT EXISTS WebshopOrders  (
    id          INTEGER IDENTITY NOT NULL,
    date        DATETIME,
    email       VARCHAR(64) NOT NULL,
    CONSTRAINT FK_Orders_Users FOREIGN KEY(email) REFERENCES WebshopUsers(email)
);

CREATE TABLE IF NOT EXISTS WebshopOrderItems  (
    id          INTEGER IDENTITY NOT NULL,
    amount      INTEGER NOT NULL,
    productId   INTEGER NOT NULL,
    orderId     INTEGER NOT NULL,
    CONSTRAINT FK_OrderItems_Products FOREIGN KEY(productId) REFERENCES Products(id),
    CONSTRAINT FK_OrderItems_Orders FOREIGN KEY(orderId) REFERENCES WebshopOrders(id)
);
