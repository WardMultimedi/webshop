-- HyperSQL
TRUNCATE TABLE WebshopOrderItems RESTART IDENTITY AND COMMIT;
TRUNCATE TABLE WebshopOrders RESTART IDENTITY AND COMMIT;
TRUNCATE TABLE Products RESTART IDENTITY AND COMMIT;
TRUNCATE TABLE WebshopUsers RESTART IDENTITY AND COMMIT;
TRUNCATE TABLE ProductCategories RESTART IDENTITY AND COMMIT;
TRUNCATE TABLE Brands RESTART IDENTITY AND COMMIT;

INSERT INTO ProductCategories(id, description, name)
VALUES (1, null, 'laptops'),
       (2, null, 'smartphones'),
       (3, null, 'accessories');

INSERT INTO Brands(id, name, website)
VALUES (1, 'Apple', 'www.apple.com'),
       (2, 'Samsung', null),
       (3, 'HUAWEI', null);

INSERT INTO Products(id, model, price, stock, brandId, categoryId)
VALUES (1, 'XS MAX', 1400, 20, 1, 2),
       (2, 'J5', 600, 10, 2, 2),
       (3, 'P7 Lite', 200, 30, 3, 2);

INSERT INTO WebshopUsers(email, password, firstname, lastname, street, busNr, zipcode, city, country)
VALUES ('ward@multimedi.be', 'lol', 'Ward', 'Truyen', 'javastraat', '11A', '3000', 'Leuven', 'Belgium' ),
       ('nick@multimedi.be', 'lol', 'Nick', 'Bauters', 'javastraat', '11B', '3000', 'Leuven', 'Belgium' ),
       ('boerteun@boerenbond.be', 'lol', 'Teun', 'Patat', null, null, null, null, null );

INSERT INTO WebshopOrders(id, date, email)
VALUES (1, '2010-12-31 12:30:00', 'ward@multimedi.be'),
       (2, '2012-12-31 12:30:00', 'ward@multimedi.be'),
       (3, '2013-12-31 12:30:00', 'boerteun@boerenbond.be');

INSERT INTO WebshopOrderItems(id, orderId, productId, amount)
VALUES (1, 1, 1, 1),
       (2, 2, 2, 1),
       (3, 3, 2, 1),
       (4, 3, 3, 1);
